package com.marc.JSONSimple2;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class EscriureJSON {

    public static void main(String[] args) {

        JSONObject obj = new JSONObject();
        obj.put("nom", "marc");
        obj.put("edad", 21);

        JSONArray list = new JSONArray();
        list.add("jugar al WoW");
        list.add("programar");
        list.add("riure'm dels alumnes");

        obj.put("aficions", list);

        try (FileWriter file = new FileWriter("f:\\test.json")) {

            file.write(obj.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(obj);

    }

}