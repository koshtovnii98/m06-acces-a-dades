package JAXB;



import javax.xml.bind.annotation.XmlAttribute;  
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  

@XmlRootElement 
public class Alumne {
	private int id;
	private String nom;
	private String cognom;
	private String classe;
	private int nota;
	private double altura;

	//Obligatorio - Constructor vacio
	public Alumne() {
		super();
	}

	
	public Alumne(int id, String nom, String cognom, String classe, int nota, double altura) {
		super();
		this.id = id;
		this.nom = nom;
		this.cognom = cognom;
		this.classe = classe;
		this.nota = nota;
		this.altura = altura;
	}

	//atributo - siempre
	@XmlAttribute  
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	//siempre delante del get
	@XmlElement  
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@XmlElement  
	public String getCognom() {
		return cognom;
	}

	public void setCognom(String cognom) {
		this.cognom = cognom;
	}

	@XmlElement  
	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	@XmlElement  
	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	@XmlElement 
	public double getAltura() {
		return altura;
	}


	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	
	
}