package JAXB;


import java.io.File;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;  
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;  
 

public class Xml2Modul {
	
    public static void main(String[] args) {  
    	   
        try {  
      
           File file = new File("modul");  
           JAXBContext jaxbContext = JAXBContext.newInstance(Modul.class);  
      
           Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
           Modul m6= (Modul) jaxbUnmarshaller.unmarshal(file);  
             
           System.out.println(m6.getProfessor()+" "+m6.getAls().get(0).getNom());  
           
           
           Alumne dani = new Alumne(201,"Dani","Suñe","2DAM",5,1.71);
           
           m6.setProfessor("Nicolas Torrubiano");
           m6.getAls().add(dani);
           
           Marshaller marshallerObj = jaxbContext.createMarshaller();  
           marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);  
   	    
           marshallerObj.marshal(m6, new FileOutputStream("modul-new"));  
             
           
         } catch (Exception e) {  
           e.printStackTrace();  
         }  
      
       }  

}
